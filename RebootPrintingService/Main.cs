﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RebootPrintingService
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        System.ServiceProcess.ServiceController spooler_service;
        private void Main_Shown(object sender, EventArgs e)
        {
            Microsoft.Win32.RegistryKey un_true_off = Microsoft.Win32.Registry.CurrentUser;
            un_true_off = un_true_off.OpenSubKey("SOFTWARE", true); bool entry_finded = false;
            foreach (string i in un_true_off.GetSubKeyNames())
            {
                if (i == "INPrinters{461541-75953-75739-51753797}")
                {
                    entry_finded = true;
                }
            }
            if (entry_finded)
            {
                un_true_off = un_true_off.OpenSubKey("INPrinters{461541-75953-75739-51753797}", true);
                string r_state = (string)un_true_off.GetValue("enabled");
                un_true_off.Close();
                if (r_state == "true")
                {
                    spooler_service = new System.ServiceProcess.ServiceController("Spooler");
                    if (spooler_service.Status == System.ServiceProcess.ServiceControllerStatus.Running)
                    {
                        spooler_service.Stop();
                        timer1.Start();
                    }
                    else
                    {
                        spooler_service.Close();
                        MessageBox.Show("Невозможно выполнить переопрос сетевых принтеров!", "Ошибка");
                        Close();
                    }                        
                }
                else
                {
                    MessageBox.Show("Невозможно выполнить переопрос сетевых принтеров!","Ошибка");
                    Close();
                }
            }
            else
            {
                un_true_off = un_true_off.CreateSubKey("INPrinters{461541-75953-75739-51753797}");
                un_true_off.SetValue("enabled", "true", Microsoft.Win32.RegistryValueKind.String);
                un_true_off.Close();
                MessageBox.Show("Перезапустите программу.", "Сообщение");
                Close();
            }
        }
        byte current_count = 0; byte stage = 1;
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            spooler_service.Refresh();
            if (stage == 1)
            {
                if (spooler_service.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                {
                    spooler_service.Start();
                    stage = 2;
                    progressBar1.Value = 50;
                }
                else
                {
                    if (current_count % 5 == 0) {
                        current_count++;
                        if (progressBar1.Value < 50)
                        {                            
                            progressBar1.Value++;
                            label2.Text = progressBar1.Value + "%";
                        }
                    } else { current_count++; }
                }
                timer1.Start();
            }
            else if (stage == 2)
            {
                if (spooler_service.Status == System.ServiceProcess.ServiceControllerStatus.Running)
                {
                    progressBar1.Value = 100;
                    label2.Text = "готово";
                    stage = 3;
                    timer1.Interval = 1000;
                    progressBar1.Value = 100;
                }
                else
                {
                    if (current_count % 5 == 0)
                    {
                        current_count++;
                        if (progressBar1.Value < 100)
                        {
                            progressBar1.Value++;
                            label2.Text = progressBar1.Value + "%";
                        }
                    }
                    else { current_count++; }
                }
                timer1.Start();
            }
            else if (stage == 3)
            {
                spooler_service.Close();
                Close();
            }
        }

        const int MF_BYPOSITION = 0x400;

        [System.Runtime.InteropServices.DllImport("User32")]
        private static extern int RemoveMenu(IntPtr hMenu, int nPosition, int wFlags);

        [System.Runtime.InteropServices.DllImport("User32")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [System.Runtime.InteropServices.DllImport("User32")]
        private static extern int GetMenuItemCount(IntPtr hWnd);

        private void Main_Load(object sender, EventArgs e)
        {
            IntPtr hMenu = GetSystemMenu(this.Handle, false);
            int menuItemCount = GetMenuItemCount(hMenu);
            RemoveMenu(hMenu, menuItemCount - 1, MF_BYPOSITION);
        }
    }
}
