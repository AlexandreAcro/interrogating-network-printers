﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace RebootPrintingService
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool runned;
            System.Threading.Mutex nmp = new System.Threading.Mutex(true, "{28a54849-2888-411e-a4f8-7ec6953131fa}", out runned);
            if (runned)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Main());
            }
            else
            {
                MessageBox.Show("Невозможно запустить второй экземпляр программы!","Ошибка");
            }
            
        }
    }
}
